//
//  Converter.swift
//  lab1
//
//  Created by Дмитрий Мишин on 04.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

protocol Converter {
    func converte(value: Double) -> Double;
}
