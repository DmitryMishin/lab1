//
//  ConverterModel.swift
//  lab1
//
//  Created by Дмитрий Мишин on 04.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class ConverterModel {
    var database: Database;
    
    init(_database: Database = Database()) {
        database = _database;
    }
    
    func convertValue(name: String, currency: Double) -> Double {
        return currency / database.getCourse(name: name);
    }
}
