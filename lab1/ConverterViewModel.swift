//
//  ConverterViewModel.swift
//  lab1
//
//  Created by Дмитрий Мишин on 05.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class ConverterVievModel {
    let model: ConverterModel;
    
    init() {
        model = ConverterModel();
    }
    
    func converteValue(currency: Double) -> [Double] {
        return model.convertValue(currency: currency);
    }
}
