//
//  Currency.swift
//  lab1
//
//  Created by Дмитрий Мишин on 08.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

protocol Currency {
    var value: Double {get}
    var name: String {get}
}
