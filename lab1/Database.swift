//
//  Database.swift
//  lab1
//
//  Created by Дмитрий Мишин on 08.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Database {
    let currencies: [Currency] = [Dollar(), Rub(), Euro()];
    
    func getCourse(name: String) -> Double {
        var result = currencies.filter({
            (currency: Currency) -> Bool in
            return name == currency.name;
        });
        
        return result[0].value;
    }
}
