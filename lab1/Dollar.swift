//
//  Dollar.swift
//  lab1
//
//  Created by Дмитрий Мишин on 08.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Dollar: Currency {
    var value: Double {
        get {
            return 1.922;
        }
    }
    
    var name: String {
        get {
            return "EN";
        }
    }
}
