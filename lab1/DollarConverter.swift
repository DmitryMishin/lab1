//
//  DollarConverter.swift
//  lab1
//
//  Created by Дмитрий Мишин on 05.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class DollarConverter: Converter {
    let course = 1.927;
    
    func converte(value: Double) -> Double {
        return value / course;
    }
}
