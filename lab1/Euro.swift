//
//  Euro.swift
//  lab1
//
//  Created by Дмитрий Мишин on 08.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Euro: Currency {
    var value: Double {
        get {
            return 2.049;
        }
    }
    
    var name: String {
        get {
            return "EU";
        }
    }
}
