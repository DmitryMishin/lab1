//
//  Rub.swift
//  lab1
//
//  Created by Дмитрий Мишин on 08.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Rub: Currency {
    var value: Double {
        get {
            return 0.032;
        }
    }
    
    var name: String {
        get {
            return "RUB";
        }
    }
}
