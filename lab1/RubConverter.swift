//
//  RubConverter.swift
//  lab1
//
//  Created by Дмитрий Мишин on 05.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class RubConverter: Converter {
    let course = 0.302;
    
    func converte(value: Double) -> Double {
        return value / course;
    }
}
