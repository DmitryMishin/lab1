//
//  ViewController.swift
//  lab1
//
//  Created by Дмитрий Мишин on 03.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var converterModel: ConverterModel! = ConverterModel();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var calcCurrencyButton: UIButton!
    @IBOutlet weak var currencyField: UITextField!
    @IBOutlet weak var dollarLabel: UILabel!
    @IBOutlet weak var euroLabel: UILabel!
    @IBOutlet weak var rubLabel: UILabel!

    @IBAction func handleClickCalcCurrencyButton(_ sender: Any) {
        if var currency = Double(currencyField.text!) {
            if (currency < 0) {
                showError(title: "Error", message: "Value should be more than 0");
                
                return;
            } else if (currency == 0) {
                currency = 1;
                currencyField.text = "1";
            }
            
            let dollar: Double = converterModel.convertValue(name: "EN", currency: currency);
            let rub: Double = converterModel.convertValue(name: "RUB", currency: currency);
            let euro: Double = converterModel.convertValue(name: "EU", currency: currency);
            
            dollarLabel.text = "Dollar: " + String(dollar);
            euroLabel.text = "Ruble: " + String(rub);
            rubLabel.text = "Euro: " + String(euro);
        } else {
            showError(title: "Error", message: "Error in text block");
        }
    }
    
    private func showError(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil));
        self.present(alert, animated: true, completion: nil);
    }

}

